//
//  main.cpp
//  NItest
//
//  Created by 寺門　幸英 on 2016/12/02.
//  Copyright © 2016年 寺門　幸英. All rights reserved.
//

#include <nidaqmxbase/NIDAQmxBase.h>
#include <iostream>

int main(int argc, const char * argv[]) {
    TaskHandle  taskHandle = 0;
    float64     min = -10;
    float64     max = 10;
    float64     sampleRate = 20000.0;
    float64     timeout = 10.0;
    uInt32      samplesPerChan = 100;
    int32       pointsToRead = 1000;
    int32       pointsRead;
    char        chan[] = "Dev1/ai0";
    char        clockSourse[] = "OnboardClock";
    
    DAQmxBaseCreateTask("", &taskHandle);
    DAQmxBaseCreateAIVoltageChan(taskHandle, chan, "", DAQmx_Val_RSE, min, max, DAQmx_Val_Volts, NULL);
    DAQmxBaseCfgSampClkTiming(taskHandle, clockSourse, sampleRate, DAQmx_Val_Rising, DAQmx_Val_ContSamps, samplesPerChan);
    DAQmxBaseCfgInputBuffer(taskHandle, 1000000);
    DAQmxBaseStartTask(taskHandle);
    
    double data[1000] = {0};
    int end = 0;
    
    while(true){
        DAQmxBaseReadAnalogF64(taskHandle, pointsToRead, timeout, DAQmx_Val_GroupByScanNumber, data, 1000, &pointsRead, NULL);
        
        for (int i = 0; i < 1000; i++) {
            std::cout << data[i] << std::endl;
        }
        
        end++;
        if (end == 100) {
            break;
        }
    }
    DAQmxBaseStopTask(taskHandle);
    DAQmxBaseClearTask(taskHandle);
    
    return 0;
}
